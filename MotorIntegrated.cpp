/*  CPSC 1490 - Integrated Code V1.2
    March 31, 2017
    Danny Lee & Bruno Rodriguez

    Purpose: Control 2 servos, 1 DC, 1 stepper using 2 joystick

    Reference:
      - instructions for setting up the motor shield hardware and library can be found here:
        https://learn.adafruit.com/adafruit-motor-shield-v2-for-arduino/overview

      - functions for the library can be found here:
        https://learn.adafruit.com/adafruit-motor-shield-v2-for-arduino/library-reference

    To do:                                                                                      Completion Date:
      - find code to use servo pins built onto motor shield                                     March 27, 2017
      - rename variables to make code intuitive:                                                March 29, 2017
        - pullUp
        - clawDeg
      - add individual buttons to allow movement of only 1 aspect at a time                     Obsolete

      - create variable for rotDegree speed of DC motor:
        - e.g.: track -> setSpeed(x);
        - maybe switching to 2 DC motor + 1 servo + 1 stepper
      - attach Harry's code for stepper motor

      - crare arm rotDegree:
        - current problem:                                                                       March 29, 2017
          - after rotating to 180 + let go button, holds it in place
          - when button pressed on joystick (AT NEUTRAL POSITION), boom snaps back to middle
          - how do we fix this?
        - solution:
          - removed button function => messing up basePosition
          - obtain current position
          - obtain joystick reading
            - if left turn, increment servo degree by 1
            - if right turn, decrement servo degree by 1
            - 70~110 joystick reading (from 0~180) ==> do nothing
*/

//==========================================================
//include all header files, libraries, etc here:
#include <Servo.h>
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
//==========================================================

//==========================================================
//declare all global variables here:
int rotDegree = 0,      //to store joystick input for base rotation
    trackReading = 0;   //to store joystick input for track movement

int pullUp = 0,         //to store joystick input for pull up module
    clawDeg = 0;        //to store joystick input for claw module

int prevClawPos = 90,   //to track servo position before issuing new position
    basePosition = 90;   //to track servo position before issuing new position

//stepper variables
int count = 0;
int count2 = 0;
int delayTime = 0;
//==========================================================

//==========================================================
//declare all global constants for pins here:
//pins for 2 joysticks:
const int rotDegReadingPin = A8,
          trackReadingPin = A9;

const int pullUpPin = A11,
          clawDegPin = A10;

//servo output pins:
const int basePin = 9,
          clawPin = 10;

const int baseRotationSpeed = 2,
          clawRotationSpeed = 1;

const int baseRotationRangeLimit = 10,
          clawRotationRangeLimit = 50;

//dc output pins
const int trackPin = 1,
          spoolPin = 2;

//DC motor speed
const int trackSpeed = 10,
          spoolSpeed = 10;
          
//stepper output pins
const int stepperPins[] = {24, 26, 28, 30};
//==========================================================

//==========================================================
//declare all objects here:
Servo base;
Servo claw;
Adafruit_MotorShield AFMotorShieldObject = Adafruit_MotorShield();        //create motorshield object
Adafruit_DCMotor* track = AFMotorShieldObject.getMotor(trackPin);       //create DC motor
Adafruit_DCMotor* spool = AFMotorShieldObject.getMotor(spoolPin);       //create DC motor

//==========================================================

//==========================================================
//declare all function prototypes here:
void readInputs();
void moveServo(Servo thisServo, int servoPosition, int joyStickReading, const int rotationSpeed);
void moveDC(Adafruit_DCMotor* thisDC, int joyStickReading, int DCspeed);
void moveStepper();
void moveForward();
void moveBackward();

void diagnoseJoysticks();
void diagnoseServo();
void diagnoseDC();
void diagnoseStepper();
//==========================================================

void setup() {
  //==========================================================
  //initialize all input pins here:
  pinMode(rotDegReadingPin, INPUT);
  pinMode(trackReadingPin, INPUT);

  pinMode(pullUpPin, INPUT);
  pinMode(clawDegPin, INPUT);
  //==========================================================

  //==========================================================
  //initialize all output pins here:
  //servo output pins
  base.attach(basePin);
  claw.attach(clawPin);

  //stepper motor shield output pins
  for (count = 0; count < 4; count++) {
    pinMode(stepperPins[count], OUTPUT);
  }

  //initialize Adafruit motor shield
  AFMotorShieldObject.begin();
  //==========================================================

  //==========================================================
  //initialize all diagnostic methods here:
  Serial.begin(9600);
  //==========================================================
}

void loop() {
  readInputs(); //diagnoseJoysticks();

  moveServo(base, basePosition, rotDegree, baseRotationSpeed);
  moveServo(claw, prevClawPos, clawDeg, clawRotationSpeed);

  moveDC(track, trackReading, trackSpeed);
  moveDC(spool, pullUp, spoolSpeed);
}

//==========================================================
//insert all helper functions here:
//  Purpose: read joystick inputs here and calibrate accordingly
void readInputs() {
  //read joystick values for servos
  rotDegree = analogRead(rotDegReadingPin),
  clawDeg = analogRead(clawDegPin);

  //read joystick values for DC
  trackReading = analogRead(trackReadingPin),
  pullUp = analogRead(pullUpPin);

  //calibration:
  rotDegree = map(rotDegree, 0, 1023, 0, 180);                    //base rotDegree mapping for servo
  clawDeg = map(clawDeg, 0, 1023, 0, 180);                        //claw pinch mapping for servo
  trackReading = map(trackReading, 0, 1023, 0, 255);              //track DCMotorSpeed Mapping
  pullUp = map(pullUp, 0, 1023, 0, 255);                           //spool DCMotorSpeed Mapping
}

//  Purpose: move servo according to joystick input
void moveServo(Servo thisServo, int servoPosition, int joyStickReading, const int rotationSpeed) {
  servoPosition = thisServo.read();     //obtain servo position

  if (joyStickReading < 70) {                         //if joystick says ccw, turn ccw by 1 degree
    thisServo.write(servoPosition - rotationSpeed);
  }
  if (joyStickReading > 110) {
    thisServo.write(servoPosition + rotationSpeed);   //if joystick says cw, turn ccw by 1 degree
  }
  delay(20);
}

//  Purpose: move DC motor
void moveDC(Adafruit_DCMotor* thisDC, int joyStickReading, const int DCspeed) {
  thisDC -> setSpeed(DCspeed);
  if (joyStickReading > 124 + 20) {
    thisDC -> run(FORWARD);
  }
  else if (joyStickReading < 124 - 20) {
    thisDC -> run(BACKWARD);
  }
  else {
    thisDC -> run(RELEASE);
  }
}

//  Purpose: display joystick reading values on serial monitor
void diagnoseJoysticks() {
  String rotDegreeReading = "Rotation reading: ",
         trackReadingReading = "\tTrack reading: ";

  String clawDegReading = "\t\tClaw reading: ",
         pullUpReading = "\tPull up reading: ";

  rotDegreeReading += String(rotDegree);
  trackReadingReading += String(trackReading);


  clawDegReading += String(clawDeg);
  pullUpReading += String(pullUp);

  Serial.println(rotDegreeReading + trackReadingReading  + clawDegReading + pullUpReading);
}

//  Purpose: dignose rotateBase()
void diagnoseServo() {
  String a = "rotation degree: ";
  a += String(rotDegree);
  String s = "\t\tprevious Base Position: ";
  s += String(basePosition);
  Serial.println(a + s);
}

//  Purpose: diagnose moveDC()
void diagnoseDC() {
  String s = "Track Reading: ";
  s += String(trackReading);
  Serial.println(s);
}

//  Purpose: diagnose moveStepper()
void diagnoseStepper() {

}

